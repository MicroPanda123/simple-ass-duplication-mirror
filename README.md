# Simple ass duplicating
(I just wanted an acronym)

sad-dd is a simple, dd-like, utility for copying files.
This project is mostly educational for me, I wanted to learn rust more.

## Usage
It's pretty simple, like name suggests, basic usage is `sad-dd if=input_file of=output_file`. For more information run `sad-dd --help` (or check parsing.rs file for it's output)

## sad-dd vs dd-rs
sad-dd diverged from mission to be "API compatible" with dd, so I decided to make dd-rs into separate branch. I wanted to add some functionality to sad-dd that is not in dd, so I did that.

## Use cases and speed
From my [benchmarks](BENCHMARK.md) I concluded that sad is faster than both busybox dd and coreutils dd, for use in copying files and stuff. sad's usecase covers usecase of most people that use dd rly, just to write something to a drive lol, and while sad implements options like skip and seek, the biggest strenght of sad I see is just writing files to stuff.

## To-do
- Support built-in compression
- Profile and ~~improve performance~~ (performance seems good for now)
- idk whatever