# Benchmark

This is applicable for commit [7edb89ea5e](https://git.markasspandi.pl/MicroPanda123/simple-ass-duplication/commit/7edb89ea5e5c989f669256e60a4919207befef4e), future versions seem to have similar results but no promise they will be the same.

## Edit
My friend insulted me and said I couldn't write it correctly, so I rewrote this whole damn thing. Jacek, Keep Yourself Safe :) (my bf also told me to do it so ig I should)

I've decided that I wanted to compare performance of my "dd", with 2 other implementations: og coreutils one, and busybox one.

To make this test reliable, I've created a clean Linode instance (Nanode 1GB) with Debian 11 (with Trixie repositories enabled). To benchmark programs, I've used excellent benchmarking utility [hyperfine](https://github.com/sharkdp/hyperfine)

## Workspace prep
Preparing instance was pretty easy, but I made some silly decisions along the way.

I've installed Rust using official install script from rust-lang.org, Git from Debian repositories. I also installed build-essential because I've decided to compile busybox myself. A silly decision was that to install hyperfine, I've added Trixie repository to my Debian installation. Hyperfine is written in Rust, so I could just compile it myself like sad-dd, but I've noticed that after all the testing.

Anyway, I compiled busybox in version 1.36.1, using `make allnoconfig` and `make menuconfig`, because I wanted busybox that would only include dd utility (Since I didn't need other stuff for this)

Then ofc I just cloned this repo, and ran `cargo build --release`.

Before I started benchmarking, I've also moved both compiled busybox and sad-dd into designated folder "benchmark".

## Actual benchmark
### First basic benchmark
At first, I ran `hyperfine -L program ./sad-dd,"./busybox dd",dd "{program} if=/dev/urandom of=testfile count=512 bs=1M"`, this was meant for 2 things: 
- I wanted a baseline for other benchmarks;
- 2nd to create testfile that I would use for later benchmarks.

Second part did not work out because I later screwed up a little, which caused all later benchmarks to be performed on "testfile" that was of size 4GiB, not 512MiB.

This was not really a correct benchmark, it doesn't have any preparations, ignored stuff such as drive caches, etc. Even with this, results looked promising for sad-dd:
```
Benchmark 1: ./sad-dd if=/dev/urandom of=testfile count=512 bs=1M
  Time (mean ± σ):      3.515 s ±  0.310 s    [User: 0.003 s, System: 3.374 s]
  Range (min … max):    3.136 s …  4.214 s    10 runs
 
Benchmark 2: ./busybox dd if=/dev/urandom of=testfile count=512 bs=1M
  Time (mean ± σ):      3.603 s ±  0.087 s    [User: 0.004 s, System: 3.444 s]
  Range (min … max):    3.477 s …  3.786 s    10 runs
 
Benchmark 3: dd if=/dev/urandom of=testfile count=512 bs=1M
  Time (mean ± σ):      3.629 s ±  0.106 s    [User: 0.003 s, System: 3.477 s]
  Range (min … max):    3.442 s …  3.825 s    10 runs
 
Summary
  ./sad-dd if=/dev/urandom of=testfile count=512 bs=1M ran
    1.03 ± 0.09 times faster than ./busybox dd if=/dev/urandom of=testfile count=512 bs=1M
    1.03 ± 0.10 times faster than dd if=/dev/urandom of=testfile count=512 bs=1M
```
Pretty good in my opinion, especially because sad-dd was first, which theoretically should be a disadvantage (as far as I know).

### First *actual* benchmark - Urandom to Null with cold caching.
As the first actual benchmark, I've decided to simply go with default settings, reading from urandom and writing to null, bs=4M and count=1024, so all programs were reading and "writing" 4096MiB of data.
So the command I ran was `hyperfine --prepare 'sync; echo 3 | sudo tee /proc/sys/vm/drop_caches' -L program ./sad-dd,"./busybox dd",dd "{program} if=/dev/urandom of=/dev/null count=1024 bs=4M" --export-json result_4096MiB_urandom_null.json`

- `--prepare 'sync; echo 3 | sudo tee /proc/sys/vm/drop_caches'` - This runs the command in '' before every test run. So this command resets drive cache, which means that every run was running on cold cache.
- `--export-json result_4096MiB_urandom_null.json` - Probably pretty self-explanatory but, it saves results to json file.

And results look like this:
```
Summary
  dd if=/dev/urandom of=/dev/null count=1024 bs=4M ran
    1.00 ± 0.02 times faster than ./sad-dd if=/dev/urandom of=/dev/null count=1024 bs=4M
    1.01 ± 0.03 times faster than ./busybox dd if=/dev/urandom of=/dev/null count=1024 bs=4M
```

Well, coreutils beat me here (barely :p), and I still beat busybox (also barely, but less barely than coreutils did with me).
But this one doesn't really represent real life scenarios, now, does it?

Let's do that right now.
### Second benchmark(s) - Copying files, both with warm and cold cache
In this benchmark, we will copy the testfile (file that was meant to be created earlier, but like I've told you, it was not the one lol) to a different file, originally called "testfile2".
This time, I'll do this 2 different ways, one will be using cold cache, so, like previously, running cache cleaning command before each run, and the other one will use warm cache, meaning before the actual benchmark, benchmark command will be run 3 times, this will "warm up" the cache, and probably cause other stuff I don't know.


So first I've run `hyperfine --prepare 'sync; echo 3 | sudo tee /proc/sys/vm/drop_caches' -L program ./sad-dd,"./busybox dd",dd "{program} if=testfile of=testfile2 bs=4M" --export-json result_cold_testfile_to_testfile2.json`

Results were truly astonishing:
```
Summary
  ./sad-dd if=testfile of=testfile2 bs=4M ran
    1.01 ± 0.08 times faster than ./busybox dd if=testfile of=testfile2 bs=4M
    1.01 ± 0.11 times faster than dd if=testfile of=testfile2 bs=4M
```
Yay! sad-dd beats both competitors once again by huge margin, incredible. Now let's check warm cache.

For warm cache, we replace `--prepare` argument with `--warmup 3`.
So full command was: `hyperfine --warmup 3 -L program ./sad-dd,"./busybox dd",dd "{program} if=testfile of=testfile2 bs=4M" --export-json result_warm_testfile_to_testfile2.json`.

This time, results are actually more impressive:
```
Summary
  ./sad-dd if=testfile of=testfile2 bs=4M ran
    1.13 ± 0.07 times faster than ./busybox dd if=testfile of=testfile2 bs=4M
    1.16 ± 0.07 times faster than dd if=testfile of=testfile2 bs=4M
```
sad turned out .13 times faster than busybox, AND .16 times faster than coreutils? Yeah, seemed impossible for me as well. So just to be sure, I re-ran this benchmark again.
```
Summary
  ./sad-dd if=testfile of=testfile2 bs=4M ran
    1.02 ± 0.08 times faster than ./busybox dd if=testfile of=testfile2 bs=4M
    1.05 ± 0.09 times faster than dd if=testfile of=testfile2 bs=4M
```
Now, results aren't that impressive anymore, but sad won again (slightly).

### Third benchmark(s) - buffer size fun
From now on, all tests were done on cold cache, for simplicity.

This time I've done the same thing as previously, aka copied files, but this time I've checked speed over different buffer sizes. 

I compared 8 different buffer sizes: 4K, 16K, 128K, 512K, 1M, 4M, 16M, 128M, so this is the command: `hyperfine --prepare 'sync; echo 3 | sudo tee /proc/sys/vm/drop_caches' -L bs 4K,16K,128K,512K,1M,4M,16M,128M -L program ./sad-dd,"./busybox dd",dd "{program} if=testfile of=testfile2 bs={bs}" --export-json result_buffer_sizes.json`

How did it go?
```
Summary
  ./sad-dd if=testfile of=testfile2 bs=128M ran
    1.14 ± 0.12 times faster than ./busybox dd if=testfile of=testfile2 bs=128M
    1.16 ± 0.08 times faster than dd if=testfile of=testfile2 bs=128M
[...]
    1.61 ± 0.15 times faster than dd if=testfile of=testfile2 bs=4K
```
Seems like another win for sad-dd, still not by a lot, but win nonetheless.

### Fourth benchmark - The real world test
This was the last benchmark I wanted to conduct for now. This time, to make it represent a true real life scenario, I've decided to run it on my own computer, using actual USB drives. There are certain differences between those setups:
- I am running Arch (btw), not Debian
- Busybox was not compiled by me, instead installed from Arch repository (the same version tho)
- Different drives and stuff compared to using VPS in "da cloud".

For this test, I've used cheap USB 2.0 drive and LibreElec 11.0.1 for RPi4 image.

`hyperfine --prepare 'sync; echo 3 | sudo tee /proc/sys/vm/drop_caches' -L program sad-dd,"busybox dd",dd "{program} if=LibreELEC-RPi4.arm-11.0.1.img of=/dev/sdb bs=4M" --export-json result_librelec.json` : 
```
Summary
  sad-dd if=LibreELEC-RPi4.arm-11.0.1.img of=/dev/sdb bs=4M ran
    1.00 ± 0.01 times faster than busybox dd if=LibreELEC-RPi4.arm-11.0.1.img of=/dev/sdb bs=4M
    1.01 ± 0.02 times faster than dd if=LibreELEC-RPi4.arm-11.0.1.img of=/dev/sdb bs=4M
```
Well, this is pretty slim win for sad. Fits in margin of error, oh well, still comes out first, so I count it as a win!.

## Conclusion
As shown, sad is way faster than any other dd implementation that exists, and you should only use it. (Jk, but consider using pls)

Thanks for reading!
