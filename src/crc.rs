// Simple Ass Duplication (sad-dd) - a simple, dd-like, utility for copying files.
// Copyright (C) 2023  MicroPanda123

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use std::time::Instant;

use crate::parsing;
/// This is CRC32 implementation, as I wanted to keep it fully crate free.

pub struct Crc32 {
    crc: u32,
}

impl Crc32 {
    pub fn new() -> Crc32 {
        Crc32 { crc: 0xffffffff }
    }

    pub fn update(&mut self, data: &[u8]) {
        for byte in data {
            let byte = *byte as u32;
            self.crc ^= byte;
            for _ in 0..8 {
                let mask = if self.crc & 1 == 1 { 0xedb88320 } else { 0 };
                self.crc = (self.crc >> 1) ^ mask;
            }
        }
    }

    pub fn finalize(&self) -> u32 {
        self.crc ^ 0xffffffff
    }
}

pub fn checksum_file(f: PathBuf, eta: bool) -> Result<u32, String> {
    let mut crc32 = Crc32::new();
    let fr_result = File::open(f.clone());
    let mut fr = match fr_result {
        Ok(f) => f,
        Err(er) => return Err(er.to_string()),
    };
    let size_result = fr.metadata();
    let size = match size_result {
        Ok(s) => s.len(),
        Err(er) => return Err(er.to_string()),
    };
    if size == 0 {
        return Err(format!("File {:?} is not checksumable.", f));
    }
    let mut buf = [0u8; 4 * 1024 * 1024];
    let mut sum = 0;
    let timer = Instant::now();
    loop {
        let n = fr.read(&mut buf).unwrap();
        if n == 0 {
            if eta {
                eprintln!("");
            }
            break;
        }
        crc32.update(&buf[..n]);
        sum += n;
        if eta {
            let elapsed = timer.elapsed().as_secs_f64();
            let total_percent = sum as f64 / size as f64;
            let eta = elapsed * (size as f64 / sum as f64) - elapsed;
            let bytes_per_sec = sum as f64 / elapsed;
            eprint!(
                "Checked: {}/{} - {:.0}% - {:.0}s - {}/s - ETA: {:.0}s\r\r",
                parsing::bytes_to_val(sum as f64),
                parsing::bytes_to_val(size as f64),
                total_percent * 100.0,
                elapsed,
                parsing::bytes_to_val(bytes_per_sec),
                eta
            );
        }
    }
    Ok(crc32.finalize())
}
