// Simple Ass Duplication (sad-dd) - a simple, dd-like, utility for copying files.
// Copyright (C) 2023  MicroPanda123

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// #![feature(io_error_more)]

use std::env;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::stdin;
use std::io::stdout;
use std::io::BufRead;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::process::exit;
use std::time::Instant;

mod crc;
mod parsing;

fn args_match(args_result: Result<parsing::Config, parsing::ParsingError>) -> parsing::Config {
    match args_result {
        Err(parsing::ParsingError::BSInvalidValue) => {
            eprintln!("Failed parsing buffer size, make sure it's correct.\nFor help, try 'sad-dd --help'.");
            exit(1);
        }
        Err(parsing::ParsingError::BSWrongMultiplier) => {
            eprintln!("Wrong multiplicative suffix.\nFor help, try 'sad-dd --help'.");
            exit(1);
        }
        Err(parsing::ParsingError::UnrecognizedOperand) => {
            eprintln!("^\nUnrecognized Operand\nFor help, try 'sad-dd --help'.");
            exit(1);
        }
        Err(parsing::ParsingError::InvalidCount) => {
            eprintln!("Incorrect value in 'count', it should be only numerical.\nFor help, try 'sad-dd --help'.");
            exit(1);
        }
        Err(parsing::ParsingError::InvalidSkip) => {
            eprintln!("Incorrect value in 'skip', it should be only numerical.\nFor help, try 'sad-dd --help'.");
            exit(1);
        }
        Err(parsing::ParsingError::InvalidSeek) => {
            eprintln!("Incorrect value in 'seek', it should be only numerical.\nFor help, try 'sad-dd --help'.");
            exit(1);
        }
        Ok(res) => res,
    }
}

fn main() {
    let args_result = parsing::parse_cli(env::args().collect());
    let args = args_match(args_result);
    let mut input_buffer: BufReader<Box<dyn Read>>;
    let mut file_size: u64 = 0;
    let mut input_checksum = 0;
    if args.input.is_some() {
        let input_file_result = File::open(args.input.clone().unwrap());
        let mut input_file = match input_file_result {
            Ok(file) => file,
            Err(_) => {
                eprintln!("Failed to read input file. Check permissions, path and if object exists.\nFor help, try 'sad-dd --help'.");
                exit(2);
            }
        };
        input_file
            .seek(SeekFrom::Start(args.skip * args.input_buffer_size as u64))
            .unwrap();
        if args.custom_file_size == 0 {
            file_size = input_file.metadata().unwrap().len();
        } else {
            file_size = args.custom_file_size;
        }
        input_buffer = BufReader::with_capacity(args.input_buffer_size, Box::new(input_file));

        if args.verify {
            eprintln!("Getting CRC32 hash of input...");
            input_checksum = match crc::checksum_file(args.input.unwrap(), !args.quiet) {
                Ok(ch) => ch,
                Err(err) => {
                    eprintln!("{} Continuing without...", err);
                    0
                }
            };
            eprintln!("Input checksum: {:X}", &input_checksum);
        }
    } else {
        input_buffer = BufReader::with_capacity(args.input_buffer_size, Box::new(stdin()));
        if args.verify {
            eprintln!("CRC32 verification is unavailable for stdin. Continuing without.");
        }
    }

    let mut output_buffers: Vec<BufWriter<Box<dyn Write>>> = Vec::new();
    if let Some(ref outputs) = args.output {
        for output in outputs {
            let mut output_file_result = OpenOptions::new();
            output_file_result.create(true).write(true);
            if args.seek == 0 {
                output_file_result.truncate(true);
            }
            let mut output_file: File = match output_file_result.open(output) {
                Ok(file) => file,
                Err(_) => {
                    eprintln!("Failed to open output file. Check permissions.\nFor help, try 'sad-dd --help'.");
                    exit(2);
                }
            };
            let _ = output_file.seek(SeekFrom::Start(args.seek * args.input_buffer_size as u64));
            output_buffers.push(BufWriter::with_capacity(
                args.output_buffer_size,
                Box::new(output_file),
            ));
        }
    } else {
        output_buffers.push(BufWriter::with_capacity(
            args.output_buffer_size,
            Box::new(stdout()),
        ));
    }
    let file_size_string = parsing::bytes_to_val(file_size as f64);
    let mut total: u64 = 0;
    let timer = Instant::now();
    let mut elapsed = timer.elapsed().as_secs_f64();
    let mut count: u64 = 0;
    loop {
        let internal_buffer = input_buffer
            .fill_buf()
            .expect("Failed reading into buffer.");
        let buff_length = internal_buffer.len();
        if internal_buffer.is_empty() {
            break;
        }

        count += 1;
        for output in output_buffers.iter_mut() {
            let write_stat = output.write(internal_buffer);
            match write_stat {
                Ok(_) => (),
                Err(e) => {
                    eprintln!("{}", e.to_string());
                    exit(e.raw_os_error().unwrap_or(1));
                }
            }
        }

        input_buffer.consume(buff_length);
        total += buff_length as u64;
        if count >= (args.count) && args.count != 0 {
            break;
        }
        if args.status && !args.quiet && (elapsed as u64) < timer.elapsed().as_secs() {
            elapsed = timer.elapsed().as_secs_f64();
            let total_percent = (total as f64 / file_size as f64) * 100.0;
            let eta = elapsed * (file_size as f64 / total as f64) - elapsed;
            let bytes_per_sec = total as f64 / elapsed;
            let view_bps = parsing::bytes_to_val(bytes_per_sec);
            // Trailing spaces to that previous output won't show up when output shorten
            eprint!(
                "{}/{} - {:.0}% - {:.0}s - {}/s - ETA: {:.0}s\r\r",
                parsing::bytes_to_val(total as f64),
                file_size_string,
                total_percent,
                elapsed,
                view_bps,
                eta
            );
        }
    }
    if !args.quiet {
        eprintln!(
            "\r{} bytes ({} per output) - {}s",
            total * output_buffers.len() as u64,
            parsing::bytes_to_val(total as f64),
            timer.elapsed().as_secs_f64()
        );
        eprintln!("Flushing...");
    }
    for mut output in output_buffers {
        output.flush().expect("Failed to flush, uh oh.");
    }
    if args.verify {
        eprintln!("Verifying outputs...");
        if let Some(outputs) = args.output {
            for out in outputs {
                let output_checksum = match crc::checksum_file(out.clone(), !args.quiet) {
                    Ok(ch) => ch,
                    Err(err) => {
                        eprintln!("{} Ignoring...", err);
                        0
                    }
                };
                let equal = output_checksum == input_checksum;
                eprintln!("Checksum of {:?}: {:X} - {}", out, output_checksum, equal);
            }
        } else {
            eprintln!("CRC32 verification is unavailable for stdout.");
        }
        //eprintln!("CRC of output: {:X}", crc::checksum_file("test1".into()).unwrap());
    }
}
