// Simple Ass Duplication (sad-dd) - a simple, dd-like, utility for copying files.
// Copyright (C) 2023  MicroPanda123

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{path::PathBuf, process::exit};

const HELP_MESSAGE: &str = "
sad-dd Copyright (C) 2023 MicroPanda123
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, released under 'GPL-3.0-or-later' license. 
You are welcome to redistribute it under certain conditions.

Usage: sad-dd [OPERAND]...
    if=FILE     read from FILE (default: stdin)
    of=FILE     write to FILE (default: stdout) (can be specified multiple times)
    bs=BYTES    read up to BYTES bytes at a time (default: 4MiB)
    count=N     copy only N blocks
    seek=N      skip N bs-sized output blocks
    skip=N      skip N bs-sized input blocks
    status      show progress status (slows down copying drastically)
    quiet       don't show progress status or summary (overwrites status)
    cs=BYTES    sets custom total size (used in progress status, overwrites detected filesize)
    verify      verify that source input and output(s) are the same using CRC32 (VERY slow)
BYTES may be followed by the following multiplicative suffixes:
c=1, w=2, b=512, kB=1000, K=1024, MB=1000*1000, M=1024*1024,
GB=1000*1000*1000, G=1024*1024*1024.
Binary prefixes can be used, too: KiB=K, MiB=M, and so on.
";

#[derive(Debug, PartialEq)]
pub enum ParsingError {
    BSInvalidValue,
    BSWrongMultiplier,
    UnrecognizedOperand,
    InvalidCount,
    InvalidSkip,
    InvalidSeek,
}

#[derive(Debug, PartialEq)]
pub struct Config {
    pub input: Option<PathBuf>,
    pub output: Option<Vec<PathBuf>>,
    pub count: u64,
    pub skip: u64,
    pub seek: u64,
    pub input_buffer_size: usize,
    pub output_buffer_size: usize,
    pub custom_file_size: u64,
    pub status: bool,
    pub quiet: bool,
    pub verify: bool,
}

const SUFFIXES: [&str; 5] = ["KiB", "MiB", "GiB", "TiB", "PiB"];

impl Config {
    fn new() -> Self {
        Self {
            input: None,
            output: None,
            input_buffer_size: 4194304,
            output_buffer_size: 4194304,
            count: 0,
            skip: 0,
            seek: 0,
            custom_file_size: 0,
            status: false,
            quiet: false,
            verify: false,
        }
    }
}

pub fn bytes_to_val(size: f64) -> String {
    let mut new_size;
    let mut suffixes_index = 0;
    new_size = size / 1024.0;
    while new_size >= 1024.0 {
        new_size /= 1024.0;
        suffixes_index += 1;
    }
    format!("{:.1} {}", new_size, SUFFIXES[suffixes_index])
}

pub fn parse_bs(buffer_size: &str) -> Result<u64, ParsingError> {
    let digits_result = buffer_size.split_once(|c: char| !c.is_ascii_digit());
    let digits = match digits_result {
        Some(x) => x.0,
        None => buffer_size,
    };
    let mut letters = &buffer_size[digits.len()..].to_ascii_lowercase() as &str;
    letters = match letters.strip_suffix("ib") {
        None => letters,
        Some(x) => x,
    };

    if letters.len() > 2 {
        return Err(ParsingError::BSInvalidValue);
    }

    let number_result = digits.parse::<u64>();
    let number = match number_result {
        Ok(0) => return Err(ParsingError::BSInvalidValue),
        Ok(x) => x,
        Err(_) => {
            return Err(ParsingError::BSInvalidValue);
        }
    };
    let mut multiplier = 1;

    // This is a disgrace, will 100% replace.
    if !letters.is_empty() {
        multiplier = match letters {
            "c" => 1,
            "w" => 2,
            "b" => 512,
            "k" => 1024,
            "kb" => 1000,
            "m" => 1024 * 1024,
            "mb" => 1000 * 1000,
            "g" => 1024 * 1024 * 1024,
            "gb" => 1000 * 1000 * 1000,
            "t" => 1024 * 1024 * 1024,
            "tb" => 1000 * 1000 * 1000,
            _ => return Err(ParsingError::BSWrongMultiplier),
        };
    }
    Ok(number * multiplier)
}

pub fn parse_cli(args: Vec<String>) -> Result<Config, ParsingError> {
    let mut conf = Config::new();
    for el in args {
        let opt_val_option = el.split_once('=');
        let opt_val = match opt_val_option {
            Some(x) => x,
            None => (el.as_str(), ""),
        };
        if opt_val.0.contains("sad-dd") {
            continue;
        }
        match opt_val.0 {
            "if" => {
                conf.input = Some(opt_val.1.into());
            }
            "of" => match conf.output {
                None => {
                    conf.output = Some(vec![opt_val.1.into()]);
                }
                Some(ref mut v) => v.push(opt_val.1.into()),
            },
            "bs" => {
                let bs_result = parse_bs(opt_val.1);
                let bs = match bs_result {
                    Err(error) => return Err(error),
                    Ok(res) => res as usize,
                };
                conf.input_buffer_size = bs;
                conf.output_buffer_size = bs;
            }
            "count" => {
                let res = opt_val.1.parse();
                conf.count = match res {
                    Ok(count) => count,
                    Err(_) => return Err(ParsingError::InvalidCount),
                };
            }
            "skip" => {
                let res = opt_val.1.parse();
                conf.skip = match res {
                    Ok(skip) => skip,
                    Err(_) => return Err(ParsingError::InvalidSkip),
                };
            }
            "seek" => {
                let res = opt_val.1.parse();
                conf.seek = match res {
                    Ok(skip) => skip,
                    Err(_) => return Err(ParsingError::InvalidSeek),
                };
            }
            "status" => {
                conf.status = true;
            } // For now this is done like that so dd's "status=progress" would be compatible
            "quiet" => {
                conf.quiet = true;
            }
            "cs" => {
                let bs_result = parse_bs(opt_val.1);
                let bs = match bs_result {
                    Err(error) => return Err(error),
                    Ok(res) => res as usize,
                };
                conf.custom_file_size = bs as u64;
            }
            "verify" => {
                conf.verify = true;
            }
            "--help" => {
                println!("{}", HELP_MESSAGE);
                exit(0);
            }
            _ => {
                println!("{:?}", opt_val.0);
                return Err(ParsingError::UnrecognizedOperand);
            }
        }
    }
    Ok(conf)
}
